-- 1.
SELECT customerName FROM customers WHERE country = "Philippines";

-- 2.
SELECT customers.contactLastName, customers.contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

-- 3. 
SELECT products.productName, products.MSRP FROM products WHERE productName = "The Titanic";

-- 4.
SELECT employees.lastName, employees.firstName FROM employees WHERE email ="jfirrelli@classicmodelcars.com";

-- 5.
SELECT customers.customerName FROM customers WHERE state is NULL;

-- 6.
SELECT employees.lastName, employees.firstName, employees.email FROM employees WHERE lastName="Patterson" AND firstName="Steve";

-- 7.
SELECT customers.customerName, customers.country, customers.creditLimit FROM customers WHERE country != "USA" AND creditLimit>=3000;

-- 8.
SELECT orders.customerNumber FROM orders WHERE comments  LIKE "%DHL%";

-- 9.
SELECT * FROM productlines WHERE textDescription LIKE "%state of the art%";

-- 10.
SELECT DISTINCT customers.country FROM customers;

-- 11.
SELECT DISTINCT orders.status FROM orders;

-- 12. 
SELECT customers.customerName, customers.country FROM customers WHERE country IN ("USA", "France", "Canada");

-- 13.
SELECT employees.firstName, employees.lastName, offices.city FROM employees
	JOIN offices ON employees.officeCode = offices.officeCode WHERE city="Tokyo";

-- 14.
SELECT customers.customerName FROM customers
	JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
	WHERE lastName = "Thompson" AND firstName = "Leslie";

-- 15.
SELECT products.productName, customers.customerName FROM customers
	JOIN orders ON customers.customerNumber = orders.customerNumber
	JOIN orderdetails ON orders.orderNumber = orderdetails.orderNumber
	JOIN products ON orderdetails.productCode = products.productCode
	WHERE customerName = "Baane Mini Imports";

-- 16.
SELECT employees.firstName, employees.lastName, customers.customerName, offices.country FROM customers
	JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
	JOIN offices ON employees.officeCode = offices.officeCode
	WHERE customers.country = offices.country;

-- 17.
SELECT products.productName, products.quantityInStock FROM products
	JOIN productlines ON products.productLine = productlines.productLine 
	WHERE products.productline="planes" AND products.quantityInStock <= 1000;

-- 18.
SELECT customers.customerName FROM customers WHERE phone LIKE "%+81%";